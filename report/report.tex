\documentclass[]{IEEEtran}
% If IEEEtran.cls has not been installed into the LaTeX system files,
% manually specify the path to it like:
% \documentclass[conference]{../sty/IEEEtran}

% Your packages go here
\usepackage[utf8]{inputenc}
\usepackage{mathtools}
\usepackage{blkarray} %
\usepackage{listings} %Package to include

\markboth{T-578-ITCV Computer Vision}{}

\begin{document}

  \title{Project 2}

  \author{
    \IEEEauthorblockN{Casimir Wallwitz, Leith Hobson and Stefán Laxdal}

    \thanks{casimir21, leith21, stefanl20}{casimir21, leith21, stefanl20}
  }

  \maketitle



  \begin{abstract}
    In this paper, we describe our process of visually tracking Zebrafish in an aquarium using various mid-level methods of computer vision. We came out with two approaches: One that used a region of interest to limit the possible locations for keypoints to latch onto, the other one implementing matching of keypoints and computing the corresponding transformation to update the model. While the first method is less impressive, it is the only one that works properly (given the right circumstances).
    
  \end{abstract}

  \section{Introduction}
  \label{sec:introduction}
    Visual tracking of fish in a fish tank would seem to be a rudimentary task of computer vision, given that the fish tank has no other moving objects and is captured from multiple angles. It however seems to be a difficult task for a variety of reasons, including the inherently slippery nature of fish and their visual similarity to certain corners. Our results revolve around two separate approaches with varying levels of success. Both approaches attempt to generate and capture key-points, process the key-points descriptors and use those to determine the current position of the fish within the two-dimensional frame. Both approaches begin by using the given ground truth to give each fish a starting position, and both generate key-points and store them in the same way. Where the two approaches differ is in their use of the generated key-points and descriptors, as well as the areas in the image where the data is collected.
    The following report will have sections describing each step of the tracking process, each section sub-sectioned by the two different approaches. \\ 
    We will call the approach with the highest success rate Approach 1 and the other Approach 2.


  \section{Body}
  \label{sec:body}
  
    \subsection*{Interest points and descriptors}
    \label{subsec:Interest points and descriptors}
    \subsubsection*{Approach 1}
    In this Approach, most of our success came from specifying a region of interest initialized by the ground truth, so the interest points and descriptors only played a minor role. Within this region it was very easy for the keypoint detectors to find the high-contrast head of the fish. For performance reasons we have decided to use ORB, which was significantly faster than SIFT and did not influence the keypoint quality by much. As long as the fish do not cross each other, the contrast between tank and fish is very good and interest points are easily detected by both algorithms.
    \\
    \subsubsection*{Approach 2}
    Approach 2 begins by analysing the ground truth and capturing useful data-points for model initialization. The number of fish is counted and the an instance of a Fish model is created for each of them with corresponding x and y coordinates based on camera orientation. \\
    Once all data is collected from the first frame of the ground truth, an analysis of the first few frames is performed. This was tested with a differing number of analysis frames, namely 10, 20, 30, and 50. A slight difference was noted between 10 and 20 frames, with 20 producing a better result, but no notable difference was detected above that number. \\
    The analysis itself consists of reading the ground truth position of each fish at any given frame and collecting key-points and descriptors from an isolated region around the fish. The region itself is selected through a box-mask that surrounds the fish at approximately 50 pixels to each side of the fishes head. The frame is also cropped to produce a template of the fish, it's dimensions being 15 pixels to each side of the fishes head. The key-points, descriptors and templates are then stored in it's corresponding fishes instance for each analysis frame. 
    \\
    After the analysis has finished we start playing the video starting one frame after the last analysis frame. For each frame we capture key-points, descriptors and templates in much the same way as in the analysis stage. Each fish then is provided with the data and determines which key-points are relevant to it and which are not. This stage of the process will be described in more detail in the following sections. 
    \\
    Each key-point detection algorithm has its own detectAndCompute function that was used interchangeably throughout testing of different algorithms. The algorithms themselves produced vastly different results in terms of speed. The sif algorithm was so slow as to be unusable, where as the Orb algorithm maintained almost real time speed at the expense of some accuracy. In the end we settled on the Orb algorithm for its speed provided we could tune its parameters to capture what we needed. We found that using a higher than default scale and patch size we could capture the fish making sudden turns that would otherwise cause our tracker to loose the fish.
    
    \begin{lstlisting}[language=Python]
    cv.ORB_create(
            scaleFactor=2, nfeatures=500, 
            patchSize=31, edgeThreshold=31)
    \end{lstlisting}
    
    \newpage
    \subsection*{Match Hypothesis}
    \label{subsec:Match Hypothesis}
    \subsubsection*{Approach 1}
    Thanks to the region of interest, matching keypoints is not necessary in this method. Most keypoints naturally appear on the fishes head, so even by selecting a random one, on average both the fishes position and region of interest move in whatever direction the fishes head is moving. However, in the case of two or more fish crossing each other, the lack of matching becomes immediately noticeable as the two trackers merge into one.
    \\
    \subsubsection*{Approach 2}
    Once data has been collected from each frame and sent to each fish it is the responsibility of the fish to determine of any of the provided key-points apply to it, and to update its own x and y position if needed. The first iteration of this part used the openCV brute force matching to find suitable candidates in the provided descriptors. The matches were then sorted by distance and the lowest distance match selected as the new position of the fish. The results of this approach were effective in that the selected match would mostly belong to one of available fish, but most of the time would be matched to both fish simultaneously, and as such functioned more as a fish detector than a fish tracker. \\
    A second attempt was then made using a different matching algorithm and a more involved process in determining what is a successful match.
    \begin{figure}[h]
        \includegraphics[scale=0.225]{media/fish1.png}
         \includegraphics[scale=0.285]{media/fish2.png}
        \caption{Blue and red dots corresponding to the two fish models being tracked}
        \label{fig:my_label}
    \end{figure}
    \\
    The second attempt uses the openCV FlannBasedMatcher for finding potential matches in the provided data. All parameters were derived from recommendations in the openCV documentation.
    
    \begin{lstlisting}[language=Python]
index_params = dict(
        algorithm=FLANN_INDEX_LSH, 
        table_number=6, 
        key_size=12, 
        multi_probe_level=1)
search_params = dict(checks=50)
flann = cv.FlannBasedMatcher(
                index_params, 
                search_params)

    \end{lstlisting}
    Any given fish, having been initialized with a certain number of frames, has a stored set of key-points, descriptors and frames. When receiving a new set of descriptors to be analysed, the fish will compare this new set against every set it has already stored. The comparison is done using the flann nearest neighbour algorithm. \\
    Once a set of matches has been found the matches are filtered by comparing their relative distances and keeping or removing each member based on its relative ratio to its members.  
    
    \begin{lstlisting}[language=python]
for i, pair in enumerate(matches):
    try:
        m, n = pair
        if m.distance < 0.9*n.distance:
            good.append(m)
    except ValueError:
        pass
    \end{lstlisting}
    The ratio was initially much lower but was adjusted to its current number on account of the template matching, which requires at least four valid matches to function. Adjusting parameters to produce more matches were unsuccessful.
    \\
      
    
     \subsection*{Transformation Fitting}
    \label{subsec:Transformation Fitting}
    \subsubsection*{Approach 1}
    There was no Transformation Fitting needed, since tracking points automatically converged on the fishes head.
    \\
    
    \subsubsection*{Approach 2}
    
    
    Once four or more good matches have been found we use the templates to confirm that the matches are correct.
    To do a comparison between the query template and the fishes stored template, one image has to be warped in such a way as to match features of one fish head to another. Once such a warping has been performed we can compare the points and determine the similarities between the two images using cross correlation. \begin{figure}[h]
        \centering
        \includegraphics[scale=0.925]{media/0.jpg}
         \includegraphics[scale=0.925]{media/1.jpg}
         \includegraphics[scale=0.925]{media/10.jpg}
        \caption{Blue and red dots corresponding to the two fish models being tracked}
        \label{fig:my_label}
    \end{figure}
    \\
    The coordinates of both templates are collected and placed into the openCV findHomography function. The function uses the Ransac algorithm to randomly select a set of data-points and tries to fit them to a given model. It then determines the value of the fit by computing outliers in the given model-fitting.
    The findHomography function returns a transformation matrix that represents the transformation of one template into the other. 
    
    
    \begin{lstlisting}[language=python]
src_pts = np.float32(
    [ src_pts[m.queryIdx].pt for m in good ]
    ).reshape(-1,1, 2)
dst_pts = np.float32(
    [ kp[m.trainIdx].pt for m in good ]
    ).reshape(-1, 1, 2)
Matrix, _ = cv.findHomography(
    src_pts, 
    dst_pts, 
    cv.RANSAC, 
    1.0)
    \end{lstlisting}
    % \\
    \newpage
    Once we have the transformation matrix we can apply that to the query template and compare it to the fish template in question. This is done by performing a convolution between the warped query image onto the confirmed fish template and measuring the strength of the match. If the match is above a certain threshold then we can be fairly certain that our match is indeed a correct one and we can take its coordinates as the fishes new x and y coordinates. \\
    
    \begin{lstlisting}[language=python]
pts = np.float32([ [0,0], [0,h-1],
    [w-1,h-1], [w-1,0 ]])
    .reshape(-1, 1, 2)
dst = cv.perspectiveTransform(pts, M)

warped_image = cv.polylines(frame,
    [np.int32(dst)], 
    True, 
    255, 
    3, 
    cv.LINE_AA)
ccRes = cv.matchTemplate(self.frames[idx], 
    warped_image, 
    cv.TM_CCOEFF_NORMED)
    % \end{lstlisting}
    % \\
    This more involved approach to finding matches did however not cause an improvement in our results. 
    Our matches lost accuracy and would find fish in all parts of the room, particularly on a small corner in the top-right of the screen.
    
    \subsection*{Updating the model}
    \label{subsec:Updating the model}
    \subsubsection*{Approach 1}
    The model can simply be updated by selecting any keypoint, as most of the time, the region of interest is big enough to even account for bigger outliers so that the tracked position eventually ends up on the fishes head most of the time. Averaging the keypoints made the the results worse, as the keypoints further back were slowly dragging the region of interest away from the head. There are definitely optimizations that could have been made here, but selecting the first one worked well enough. 
    \\
    
    \subsubsection*{Approach 2}
    \label{subsec:Approach 2}
    The benefits of collecting key-points, descriptors and templates are that once a match is found with a high confidence then that match contains data that can be integrated into the fish model, giving it more data to use for future matching. If a match is confirmed yet far enough from existing data, it has some variation that would be good to have at hand. However if a match is very close then the fish already knows those data points and it does not need to be stored. Both the brute force and flann matching approaches used an upper and lower bound to handle these cases separately. \\
    However we failed in implementing such a method. Once new descriptors where added to a fish the number of new matches would grow exponentially. Within a second the models were convinced that the true x and y coordinates of the fish were on a small corner on the top-right of the screen. This was the result regardless of how much we limited the threshold for new additions.
      \\
    \subsubsection*{Data Visualisation}
      We elected to graph in 3D the movement of the fish, so that we could see our results in an additional manner, and so that we could visually compare our results to the ground truth.
      The idea was to utilise the provided camera intrinsic and references files to accurately map the fish positions back to real world co-ordinates.
      \\
      We faced some set backs in performing this calculation, and so, realising that the ground truth contained not only the real world coordinates, but also the pixel coordinates, 
      we decided to graph the pixel coordinates directly as we calculated them.
      \\
      We then used the vedo library to built an aesthetically pleasing visualisation with which we were satisfied. Unfortunately however, it was not to be, and we could simply not find a 
      way to utilise this library headlessly in the CI pipeline, as the result always appears to be required to be rendered to screen before the image can be used elsewhere. Another issue
      which we found was that when rendering two separate charts for the truth, and calculated data, we observed some strange artifacts including missing elements in the chart displayed second, 
      despite the charts supposedly being seperate instances of the library. With these setbacks, we went ahead and re-implemented the visualisation using the somewhat more standard matplotlib library
      with which we have also previously worked.
      \\
      We were for some reason unable to get the library to play nicely in exporting the generated frames to openCV, possibly simply due to limited time and so we ultimately decided to simply write
      each frame to disk, and then have openCV read it in again. This was not an ideal solution, however, it did achieve the goal of visualising our calculated results. Overall, this rendering
      solution is incredibly slow, significantly reducing the performance of our entire pipeline. With this in mind we would ultimately prefer to have removed it, however, we did feel that it
      represented our results in a visually pleasing manner, and so, kept it within the scope of this project.
      \\
      Finally, in order to visualise our algorithms, we decided to generate three frames, the top and front camera views, with the marked calculated colour coded fish position, as well as the above mentioned
      3D graph indicating their calculated positions. These three frames are then each placed next to each other in a single frame, that we are able to export as a video file at run time. 


  \newpage
  \section{Conclusion}
  \label{sec:conclusion}
  \subsubsection*{Approach 1}
    While specifying a region of interest certainly helped to provide visible results, it did side-step a few important topics that we could not solve in the other Approach. Overall, we are happy that there were at least some results to be seen and while we did not use everything we wanted to, our failed attempts at other methods still gave us some perspective on what else we could have done.
    \\
  \subsubsection*{Approach 2}
    It is hard to say what exactly went wrong with the more involved template matching strategy, but it is clear that whatever went wrong comes from a lack of knowledge of the subject. Our key-point matching got to the point where a fish could be reliably distinguished from its surroundings, but never identified as a particular fish. The purpose of the template matching was to make our matching more accurate, but it had the opposite effect. With that in mind we conclude that our approach to the template matching was specifically to blame and contains some error we were unable to find.
    \\
  \subsubsection*{Data visualisation}
    We have visualised our results by both marking each frame with each fish's calculated location, and through a 3D graph, with are then compiled into a video at run time.
  
  \section{Contribution}
  \label{sec:contribution}

    \begin{equation*}
      \begin{blockarray}{*{5}{c} l}
        \begin{block}{*{5}{>{$\footnotesize}c<{$}} l}
          KP Detection & KP Matching & Data Processing & Discussion & Report \\
        \end{block}
        \begin{block}{[*{5}{c}]>{$\footnotesize}l<{$}}
          40 & 30 & 25 & 34 & 33 & Casimir \\
          30 & 30 & 40 & 33 & 33 & Leith \\
          30 & 40 & 25 & 33 & 34 & Stefán \\
        \end{block}
      \end{blockarray}
    \end{equation*}
    

\end{document}
