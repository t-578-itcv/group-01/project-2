# #!/bin/python3

# import csv

# import numpy as np

# from Constants import BASE_FOLDER, NUM_FISH, TRUTH_PATH

# from Fish import Fish

# import json
# import copy


# class Bounds:
#     def __init__(self, tl, tr, br, bl):
#         self.topLeft = tl
#         self.topRight = tr
#         self.bottomRight = br
#         self.bottomLeft = bl

#     def __str__(self):
#         return f"TopLeft:\t{self.topLeft},\nTopRight:\t{self.topRight},\nBottomRight:\t{self.bottomRight},\nBottomLeft:\t{self.bottomLeft}"


# class CameraToWorldCoordinates:
#     def __init__(self):
#         self.readTruth()

#     def readTruth(self):
#         # Get front bounds
#         # Reference
#         self.frontBounds = Bounds(*(self.readTruthFile(
#             BASE_FOLDER + 'camF_references.json')
#         ))

#         # Get top bounds
#         # Reference
#         self.topBounds = Bounds(*(self.readTruthFile(
#             BASE_FOLDER + 'camT_references.json')
#         ))

#         # print(self.frontBounds)

#     def readTruthFile(self, filePath):
#         with open(filePath) as f:
#             reference = json.load(f)
#             return reference

#     def getWorldCoords(self, camFrontCoords, camTopCoords):

#         # X coordinate

#         frontX = self.scaleValue(
#             camFrontCoords[0], self.frontBounds.topLeft)
#         topX = self.scaleValue(camTopCoords, 0)
#         x = (frontX + topX)/2

#         y = 1
#         z = 2

#         return (x, y, z)


#     def scaleValue(val, pixelRangeLow, pixelRangeHigh, worldRangeLow, worldRangeHigh):
#         return (val-pixelRangeLow)\
#             / (pixelRangeHigh-pixelRangeLow) \
#             * (worldRangeHigh-worldRangeLow)

#     def printTest(self):
#         with open(TRUTH_PATH, mode='r') as f:

#             # reading the CSV file
#             fishCSV = csv.reader(f)

#             fishLines = []

#             for line in fishCSV:
#                 fishLines.append(line)

#             fishTruths = []
#             frameCounter = 0

#             for fishLine in fishLines[(frameCounter * NUM_FISH):(frameCounter * NUM_FISH + NUM_FISH)]:
#                 fish = Fish(fishLine)
#                 fishTruths.append(fish)

#             for fish in fishTruths:

#                 if(fish.id == 1):
#                     print("\n-------")

#                 camFrontCoords = (fish.camF_x, fish.camF_y)
#                 camTopCoords = (fish.camT_x, fish.camT_y)
#                 print("Camera    :", camFrontCoords, "\t", camTopCoords)

#                 fishCalc = copy.deepcopy(fish)
#                 coords = self.getWorldCoords(camFrontCoords, camTopCoords)
#                 fishCalc.x = coords[0]
#                 fishCalc.y = coords[1]
#                 fishCalc.z = coords[2]

#                 print("Actual    :", fish)
#                 print("Calculated:", fishCalc)
#                 exit()


# if __name__ == "__main__":
#     ctw = CameraToWorldCoordinates()
#     ctw.printTest()
