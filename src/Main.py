import sys
import cv2 as cv
import numpy as np

from Fish import Fish



cap = cv.VideoCapture("./resources/ZebraFish-03-raw.webm")
sift = cv.SIFT_create()

truth = open("./resources/gt3.txt")

fishes = []

def get_or_create_fish(fish_id, x, y, z, tx, ty, fx, fy):
    for fish in fishes:
        if fish.fish_id == fish_id: return fish

    fish = Fish(fish_id, x, y, z, tx, ty, fx, fy)
    fishes.append(fish)
    return fish


def get_roi(img, x, y, size=40):
    # Create region of interest mask around x and y coordinates
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    gray[:] = 0
    gray[y-40:y+size, x-40:x+size] = 255
    return gray


while True:
    ret, frame = cap.read()
    frame = cv.resize(frame, (frame.shape[1]//2, frame.shape[0]//2), interpolation=cv.INTER_AREA)
    if frame is None:
        print("Frame not found. Exiting")
        sys.exit()
    
     
    frameNr, id1, x1, y1, z1, camT_x1, camT_y1, camT_left1, camT_top1, camT_width1, camT_height1, camT_occlusion1, camF_x1, camF_y1, camF_left1, camF_top1, camF_width1, camF_height1, camF_occlusion1 = (truth).readline().split(',')
    frameNr, id2, x2, y2, z2, camT_x2, camT_y2, camT_left2, camT_top2, camT_width2, camT_height2, camT_occlusion2, camF_x2, camF_y2, camF_left2, camF_top2, camF_width2, camF_height2, camF_occlusion2 = (truth).readline().split(',')
    
    x1 = float(x1)
    y1 = float(y1)
    z1 = float(z1)
    camF_x1 = int(camF_x1) // 4 
    camF_y1 = int(camF_y1) // 4
    camF_left1 = int(camF_left1) // 4
    camF_width1 = int(camF_width1) // 4
    camF_top1 = int(camF_top1) // 4
    camF_height1 = int(camF_height1) // 4

    #cv.rectangle(frame, (camF_x1-40, camF_y1-40), (camF_x1+40, camF_y1+40), (0, 255, 0), 4)
    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)

#    gray[:] = 0
#    gray[camF_y1-40:camF_y1+40, camF_x1-40:camF_x1+40] = 255
    roi = get_roi(frame, camF_x1, camF_y1)    
    
    kp, des = sift.detectAndCompute(frame,roi)

    for p in kp:
        print(p.pt, (camF_x1, camF_y1))
    
     
    
    
    #cv.circle(frame, (round(camF_x1),round(camF_y1)), 10, (0, 255, 0), 4)
    cv.drawKeypoints(frame, kp, frame, (255, 0, 0))


    cv.imshow('frame', frame)

    key = cv.waitKey(30)
    if key == 27: break


cap.release()
cv.destroyAllWindows()
truth.close()
