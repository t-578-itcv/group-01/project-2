import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import numpy as np

from Constants import TRUTH_PATH, VISUALISATION_PATH, MAX_VISUALISED_POINTS


class Visualizer:
    def __init__(self):
        self.points = dict()
        self.fishColours = ['tab:red', 'tab:green']

    def test(self):

        points = dict()
        for l in open(TRUTH_PATH):
            l = l.strip().split(',')
            frame, f_id, posX, posY, posZ = l[:5]
            f_id, posX, posY, posZ = int(f_id), float(
                posX), float(posY), float(posZ)
            if f_id not in points:
                points[f_id] = np.array([[posX, posY, posZ]])
            else:
                points[f_id] = np.append(
                    points[f_id], [[posX, posY, posZ]], axis=0)

        for i in range(len(points[1])):
            for j in range(1, 3):
                self.setPoint(j, points[j][i])
            self.savePlot()

    def setPoint(self, f_id, pos):
        if f_id not in self.points:
            self.points[f_id] = [
                np.array(pos[0]), np.array(pos[1]), np.array(pos[2])]
        else:
            for i in range(3):
                self.points[f_id][i] = np.append(
                    self.points[f_id][i], pos[i])

            if(self.points[f_id][0].size > MAX_VISUALISED_POINTS):
                for i in range(3):
                    self.points[f_id][i] = np.delete(self.points[f_id][i], 0)

    def savePlot(self):

        fig = plt.figure(figsize=(10, 7))
        ax = fig.add_subplot(projection='3d')

        for i in range(1, 3):
            ax.plot(self.points[i][0], self.points[i]
                    [1], self.points[i][2], color=self.fishColours[i-1])

        plt.savefig(fname=VISUALISATION_PATH)

        plt.close()


if __name__ == '__main__':

    Visualizer().test()
