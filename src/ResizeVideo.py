#!/bin/python3

import sys
import time

# importing libraries
import cv2 as cv


import numpy as np

from Constants import KEY_Q, KEY_ESC, INPUT_SCALE

videoToRescale = '/../input/labels/train/ZebraFish-01/ZebraFish-01-raw'
videoExtension = '.webm'
videoFrameRate = 60
outputFrameRate = 60


class VideoPlayer:

    def main(self):
        videoPath = sys.path[0] + videoToRescale

        # Create a VideoCapture object and read from input file
        cap = cv.VideoCapture(videoPath+videoExtension)

        # Check if camera opened successfully
        if (cap.isOpened() == False):
            print("Error opening video file: ", videoPath+videoExtension)

        firstFrame = True

        # Capture frame-by-frame
        ret, frame = cap.read()
        if ret == True:

            downscaledFrame = cv.resize(frame, None, fx=INPUT_SCALE,
                                        fy=INPUT_SCALE, interpolation=cv.INTER_AREA)

            [frameHeight, frameWidth, _] = np.shape(downscaledFrame)

        else:
            print("Error resizing video")
            exit()

        out = cv.VideoWriter(videoPath + '-' + str(INPUT_SCALE) + '.avi', cv.VideoWriter_fourcc(
            'M', 'J', 'P', 'G'), outputFrameRate, (frameWidth, frameHeight))

        # Read until video is completed
        while(cap.isOpened()):

            if(not firstFrame):
                # Capture frame-by-frame
                ret, frame = cap.read()
            else:
                firstFrame = False
            if ret == True:

                downscaledFrame = cv.resize(frame, None, fx=INPUT_SCALE,
                                            fy=INPUT_SCALE, interpolation=cv.INTER_AREA)

                cv.imshow("Original", frame)
                cv.imshow("Downscaled", downscaledFrame)

                # ESC or Q to exit
                c = cv.waitKey(1)
                if c == KEY_ESC or c == KEY_Q:
                    break

                # Write downscaled frame to file
                out.write(downscaledFrame)

                [frameHeight, frameWidth, _] = np.shape(downscaledFrame)

            # Break the loop
            else:
                break

        # When everything done, release
        # the video capture object
        cap.release()
        out.release()

        cv.destroyAllWindows()


if __name__ == "__main__":
    VideoPlayer().main()
