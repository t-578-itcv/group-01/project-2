import sys
import time
import cv2 as cv
import numpy as np
from Fish import Fish
import Constants as CONST

# HSL threshold based on OpenCV docs: https://docs.opencv.org/3.4/da/d97/tutorial_threshold_inRange.html

max_value = 255
max_value_H = 360//2
low_H = 3
low_S = 29
low_V = 17
high_H = 80
high_S = 115
high_V = 161
window_detection_name = 'Object Detection'

low_H_name = 'Low H'
low_S_name = 'Low S'
low_V_name = 'Low V'
high_H_name = 'High H'
high_S_name = 'High S'
high_V_name = 'High V'


def on_low_H_thresh_trackbar(val):
    global low_H
    global high_H
    low_H = val
    low_H = min(high_H-1, low_H)
    cv.setTrackbarPos(low_H_name, window_detection_name, low_H)


def on_high_H_thresh_trackbar(val):
    global low_H
    global high_H
    high_H = val
    high_H = max(high_H, low_H+1)
    cv.setTrackbarPos(high_H_name, window_detection_name, high_H)


def on_low_S_thresh_trackbar(val):
    global low_S
    global high_S
    low_S = val
    low_S = min(high_S-1, low_S)
    cv.setTrackbarPos(low_S_name, window_detection_name, low_S)


def on_high_S_thresh_trackbar(val):
    global low_S
    global high_S
    high_S = val
    high_S = max(high_S, low_S+1)
    cv.setTrackbarPos(high_S_name, window_detection_name, high_S)


def on_low_V_thresh_trackbar(val):
    global low_V
    global high_V
    low_V = val
    low_V = min(high_V-1, low_V)
    cv.setTrackbarPos(low_V_name, window_detection_name, low_V)


def on_high_V_thresh_trackbar(val):
    global low_V
    global high_V
    high_V = val
    high_V = max(high_V, low_V+1)
    cv.setTrackbarPos(high_V_name, window_detection_name, high_V)


class Tank:
    def __init__(self, video_path, truth_path):
        self.fishes = []
        self.alg = cv.ORB_create(
            scaleFactor=2, nfeatures=500, patchSize=31, edgeThreshold=31)
        self.truth = open(truth_path)
        self.video_path = video_path
        self.cap = cv.VideoCapture(video_path)
        self.fish_count = 0
        self.START_FRAMES = 30

        # Threshold adjustment
        cv.namedWindow(window_detection_name)

        cv.createTrackbar(low_H_name, window_detection_name, low_H,
                          max_value_H, on_low_H_thresh_trackbar)
        cv.createTrackbar(high_H_name, window_detection_name, high_H,
                          max_value_H, on_high_H_thresh_trackbar)
        cv.createTrackbar(low_S_name, window_detection_name, low_S,
                          max_value, on_low_S_thresh_trackbar)
        cv.createTrackbar(high_S_name, window_detection_name, high_S,
                          max_value, on_high_S_thresh_trackbar)
        cv.createTrackbar(low_V_name, window_detection_name, low_V,
                          max_value, on_low_V_thresh_trackbar)
        cv.createTrackbar(high_V_name, window_detection_name, high_V,
                          max_value, on_high_V_thresh_trackbar)

    def get_or_create_fish(fish_id, x, y, z, tx, ty, fx, fy):
        for fish in fishes:
            if fish.fish_id == fish_id:
                return fish

        fish = Fish(fish_id, x, y, z, tx, ty, fx, fy)
        fishes.append(fish)
        return fish

    def get_roi(self, img, x, y, size=50, type='s'):
        if type == 's':
            # Create region of interest mask around x and y coordinates
            x = round(x)
            y = round(y)
            gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            gray[:] = 0
            gray[y-size:y+size, x-size:x+size] = 255
            return gray
        elif type == 'c':
            frame_HSV = cv.cvtColor(img, cv.COLOR_BGR2HSV)
            frame_threshold = cv.inRange(
                frame_HSV, (low_H, low_S, low_V), (high_H, high_S, high_V))
            return frame_threshold

    def count_fish(self):
        seen_fishes = []
        curFish = 0
        while curFish not in seen_fishes:
            seen_fishes.append(curFish)
            frame, curFish, *_ = self.truth.readline().split(',')

        self.fish_count = len(seen_fishes) - 1
        self.truth.seek(0)

    def initialize_fish(self, lines):
        for i in range(self.fish_count):
            fish = Fish(lines[i])
            self.fishes.append(fish)

    def get_lines_and_close(self):
        lines = []
        for _ in range(self.fish_count * self.START_FRAMES):
            lines.append(self.truth.readline().split(','))

        return lines

    def get_fish_by_id(self, fish_id):
        for fish in self.fishes:
            if str(fish.id) == str(fish_id):
                return fish

        raise Exception("Fish not found (FNF)")

    def get_template(self, frame, x, y):
        x = int(x)
        y = int(y)
        frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        return frame[y-20:y+20, x-20:x+20]

    def get_x_and_y_from_line(self, line):
        # TODO Change to work for both orientations
        return (float(line[CONST.F_X]), float(line[CONST.F_Y]))

    def analyze_first_frames(self, lines, DISPLAY=True):
        print("Collecting data...")
        frameNr = 0

        while frameNr < self.START_FRAMES:
            ret, frame = self.cap.read()
            frame_lines = []
            x = y = None
            for _ in range(self.fish_count):
                line = lines[0]
                lines.remove(line)
                frame_lines.append(line)

                x, y = self.get_x_and_y_from_line(line)

                roi = self.get_roi(frame, x, y)
                kp, des = self.alg.detectAndCompute(frame, roi)

                fish = self.get_fish_by_id(line[CONST.ID])
                tx = int(x)
                ty = int(y)

                template = self.get_template(frame, tx, ty)
                # template = frame[ty-15:ty+15, tx-15:tx+15] # Fish heads

                fish.add_descriptor(des, template, kp)

                if DISPLAY:
                    cv.drawKeypoints(frame, kp, frame, (0, 255, 0))
                    #cv.imshow("roi", roi)
                    #cv.imshow("frame", frame)
                    self.resize_and_display(roi)
                    self.resize_and_display(frame)
                    # ESC or Q to exit
                    c = cv.waitKey(1)
                    if c == CONST.KEY_ESC or c == CONST.KEY_Q:
                        break
                    # Any other key to slow playback
                    elif(c > 0):
                        time.sleep(0.5)

            frameNr += 1

    def play(self):
        print("Running program...")
        while True:
            ret, frame = self.cap.read()

            if not ret:
                print("Frame not found.")
                sys.exit()

            #cv.drawKeypoints(frame, kp, frame, (255, 0, 0))
            for fish in self.fishes:
                roi = self.get_roi(frame, fish.camF_x, fish.camF_y)
                self.resize_and_display(roi, window_detection_name)
                kp, des = self.alg.detectAndCompute(frame, roi)
                fish.check_descriptor(des, kp, self.get_template(
                    frame, fish.camF_x, fish.camF_y))

            for fish in self.fishes:
                if fish.id == 1:
                    color = (255, 0, 0)
                else:
                    color = (0, 0, 255)
                cv.circle(frame, (int(fish.camF_x),
                          int(fish.camF_y)), 8, color, 2)

            #cv.imshow("frame", frame)
            self.resize_and_display(frame)
            key = cv.waitKey(30)
            if key == 27:
                break

    def resize_and_display(self, frame, name="fff"):
        shape = frame.shape
        frame = cv.resize(frame, (frame.shape[1] // 2, frame.shape[0] // 2))
        cv.imshow(name, frame)

    def teardown(self):
        self.truth.close()
        self.cap.release()
        cv.destroyAllWindows()

    def main(self):
        self.count_fish()
        lines = self.get_lines_and_close()
        self.initialize_fish(lines)
        self.analyze_first_frames(lines)
        self.play()
        self.teardown()


if __name__ == "__main__":
    Tank(CONST.VIDEO_FRONT_PATH, CONST.TRUTH_PATH).main()
