#!/bin/python3

import sys
import time

# importing libraries
import cv2 as cv

import csv

import numpy as np

from Fish import Fish

from Constants import KEY_Q, KEY_ESC, NUM_FISH, READ_TRUTH, INPUT_SCALE, DISPLAY_SCALE, BASE_FOLDER, VIDEO_FRONT_PATH, VIDEO_TOP_PATH, TRUTH_PATH, FISH_COLOURS


inputFolder = 'ZebraFish-01/'


class VideoPlayer:

    def main(self):

        # Create a VideoCapture object and read from input file
        frontCap = cv.VideoCapture(VIDEO_FRONT_PATH)
        topCap = cv.VideoCapture(VIDEO_TOP_PATH)

        # Check if file opened successfully
        if (frontCap.isOpened() == False):
            print("Error opening video file: ", VIDEO_FRONT_PATH)
        if (topCap.isOpened() == False):
            print("Error opening video file: ", VIDEO_TOP_PATH)

        frameCounter = 0

        if(READ_TRUTH):
            f = open(TRUTH_PATH, mode='r')

            # reading the CSV file
            fishCSV = csv.reader(f)

            fishLines = []

            for line in fishCSV:
                fishLines.append(line)

        # Read until video is completed
        while(frontCap.isOpened() and topCap.isOpened()):

            if(READ_TRUTH):
                fishTruths = []

                for fishLine in fishLines[(frameCounter * NUM_FISH):(frameCounter * NUM_FISH + NUM_FISH)]:
                    fish = Fish(fishLine)
                    fishTruths.append(fish)

            # Capture frame-by-frame
            retTop, top = topCap.read()
            retFront, front = frontCap.read()
            if retTop == retFront == True:

                # ESC or Q to exit
                c = cv.waitKey(1)
                if c == KEY_ESC or c == KEY_Q:
                    break
                # Any other key to slow playback
                elif(c > 0):
                    time.sleep(0.5)

                markedFishFront = front
                markedFishTop = top

                if(READ_TRUTH):
                    i = 0
                    for fish in fishTruths:

                        fish.draw_fish(FISH_COLOURS[i], markedFishFront,
                                       markedFishTop)
                        i += 1

                markedFishFront = cv.resize(markedFishFront, None, fx=DISPLAY_SCALE,
                                            fy=DISPLAY_SCALE, interpolation=cv.INTER_AREA)

                markedFishTop = cv.resize(markedFishTop, None, fx=DISPLAY_SCALE,
                                          fy=DISPLAY_SCALE, interpolation=cv.INTER_AREA)

                cv.imshow("Front", markedFishFront)
                cv.imshow("Top", markedFishTop)

                frameCounter += 1

            # Break the loop
            else:
                break

        # When everything done, release
        # the video capture object
        frontCap.release()
        topCap.release()

        cv.destroyAllWindows()

        if(READ_TRUTH):
            f.close()


if __name__ == "__main__":
    VideoPlayer().main()
