import sys

FRAME = 0
ID = 1
X = 2
Y = 3
Z = 4
T_X = 5
T_Y = 6
T_L = 7
T_T = 8
T_W = 9
T_H = 10
T_O = 11
F_X = 12
F_Y = 13
F_L = 14
F_T = 15
F_W = 16
F_H = 17
F_O = 18

KEY_Q = 113
KEY_ESC = 27

INPUT_SCALE = 1.0
DISPLAY_SCALE = 0.25

NUM_FISH = 2

READ_TRUTH = True

inputFolder = 'ZebraFish-03/'
BASE_FOLDER = sys.path[0] + '/../input/labels/train/' + inputFolder
BASE_FOLDER_OUT = sys.path[0] + '/../output/'

VIDEO_FRONT_PATH = BASE_FOLDER + 'imgF.mp4'
VIDEO_TOP_PATH = BASE_FOLDER + 'imgT.mp4'
TRUTH_PATH = BASE_FOLDER + 'gt/gt.txt'

VISUALISATION_PATH = BASE_FOLDER_OUT + '3DVisualisation/visualisation.png'
VIDEO_PATH = BASE_FOLDER_OUT + 'video/visualisation.avi'

MAX_VISUALISED_POINTS = 60

FISH_COLOURS = [(10, 10, 10), (200, 10, 200)]

FRAME_X = 2704
FRAME_Y = 1520

DISPLAY = False
