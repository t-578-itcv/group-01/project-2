
import cv2 as cv
import numpy as np

from Constants import INPUT_SCALE


class Fish:
    def __init__(self, details):
        (self.frameNr,
         self.id,
         self.x,
         self.y,
         self.z,
         self.camT_x,
         self.camT_y,
         self.camT_left,
         self.camT_top,
         self.camT_width,
         self.camT_height,
         self.camT_occlusion,
         self.camF_x,
         self.camF_y,
         self.camF_left,
         self.camF_top,
         self.camF_width,
         self.camF_height,
         self.camF_occlusion) \
            = details

        self.scaleFactor = 1/INPUT_SCALE

        self.frameNr = int(self.frameNr)
        self.id = int(self.id)
        self.x = float(self.x)
        self.y = float(self.y)
        self.z = float(self.z)
        self.camT_x = int(self.camT_x)
        self.camT_y = int(self.camT_y)
        self.camT_left = int(self.camT_left)
        self.camT_top = int(self.camT_top)
        self.camT_width = int(self.camT_width)
        self.camT_height = int(self.camT_height)
        self.camT_occlusion = int(self.camT_occlusion)
        self.camF_x = int(self.camF_x)
        self.camF_y = int(self.camF_y)
        self.camF_left = int(self.camF_left)
        self.camF_top = int(self.camF_top)
        self.camF_width = int(self.camF_width)
        self.camF_height = int(self.camF_height)

        self.camF_occlusion = bool(self.camF_occlusion)

        self.descriptors = []
        self.frames = []
        self.keypoints = []

    def __str__(self):
        return f"Fish: {self.id}, Position: {self.x} : {self.y} : {self.z}"

    def __repr__(self):
        return f"Fish: {self.id}, Position: {self.x} : {self.y} : {self.z}"

    def getBoundsFront(self):
        return (
            # Top left corner
            (round(self.camF_left/self.scaleFactor),
             round(self.camF_top/self.scaleFactor)),

            # Bottom right corner
            (round((self.camF_left+self.camF_width)/self.scaleFactor),
             round((self.camF_top+self.camF_height)/self.scaleFactor)),)

    def getBoundsTop(self):
        return (
            # Top left corner
            (round(self.camT_left/self.scaleFactor),
             round(self.camT_top/self.scaleFactor)),

            # Bottom right corner
            (round((self.camT_left+self.camT_width)/self.scaleFactor),
             round((self.camT_top+self.camT_height)/self.scaleFactor)),)

    def check_descriptor(self, desc, kp, frame):
        FLANN_INDEX_LSH = 6
        index_params = dict(algorithm=FLANN_INDEX_LSH,
                            table_number=6, key_size=12, multi_probe_level=1)
        search_params = dict(checks=50)

        flann = cv.FlannBasedMatcher(index_params, search_params)

        for idx, fDesc in enumerate(self.descriptors):
            try:
                matches = flann.knnMatch(fDesc, desc, k=2)
            except:
                return
            good = []
            for i, pair in enumerate(matches):
                try:
                    m, n = pair
                    if m.distance < 0.9*n.distance:  # should probably be 0.6 or 0.7
                        good.append(m)
                except ValueError:
                    pass

        if len(good) > 3:
            src_pts = self.keypoints[idx]
            src_pts = np.float32(
                [src_pts[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
            dst_pts = np.float32(
                [kp[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

            M, _ = cv.findHomography(src_pts, dst_pts, cv.RANSAC, 1.0)

            if M is None:
                return (self.camF_x, self.camF_y)

            h, w = frame.shape
            pts = np.float32([[0, 0], [0, h-1], [w-1, h-1],
                             [w-1, 0]]).reshape(-1, 1, 2)
            dst = cv.perspectiveTransform(pts, M)

            warped_image = cv.polylines(
                frame, [np.int32(dst)], True, 255, 3, cv.LINE_AA)

            ccRes = cv.matchTemplate(
                self.frames[idx], warped_image, cv.TM_CCOEFF_NORMED)

            if ccRes > -0.8:
                match_idx = good[0].trainIdx  # Select random match
                for match in good:
                    if False and 50 < match.distance:
                        print("MATCH")
                        self.frames.append(frame)
                        self.descriptors.append(desc)
                        self.keypoints.append(kp)

                x, y = kp[match_idx].pt
                self.camF_x = x
                self.camF_y = y

            cv.imshow('fishImg', self.frames[idx])
            cv.imshow('orgFrame', frame)
            cv.imshow('Warp', warped_image)

        return (self.camF_x, self.camF_y)

    def draw_fish(self, front, top, scale, colour=(10, 10, 255)):

        scaleFactor = 1/scale

    def add_descriptor(self, des, frame, kp):
        self.descriptors.append(des)
        self.frames.append(frame)
        self.keypoints.append(kp)

    def draw_fish(self, colour=(10, 10, 255), front=None, top=None):

        if front is not None:
            # Draw fish head from FRONT
            cv.circle(front, (round(self.camF_x/self.scaleFactor),
                              round(self.camF_y/self.scaleFactor)), 5, colour, 4)

            # Draw fish bounding box from FRONT
            frontBounds = self.getBoundsFront()
            cv.rectangle(front,
                         frontBounds[0],
                         frontBounds[1],
                         colour, 2)

        if top is not None:
            # Draw fish head from TOP
            cv.circle(top, (round(self.camT_x/self.scaleFactor),
                            round(self.camT_y/self.scaleFactor)), 5, colour, 4)

            # Draw fish bounding box from TOP
            topBounds = self.getBoundsTop()
            cv.rectangle(top,
                         topBounds[0],
                         topBounds[1],
                         colour, 2)
