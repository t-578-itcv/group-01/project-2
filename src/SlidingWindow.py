#!/bin/env python3
import cv2 as cv
import sys

import time
from Constants import FISH_COLOURS, KEY_ESC, KEY_Q, FRAME_X, FRAME_Y, DISPLAY, VISUALISATION_PATH, VIDEO_PATH, BASE_FOLDER_OUT
from collections import deque
from Visualization import Visualizer as vis
import numpy as np
DIR = sys.path[0]


class fish:
    def __repr__(self):
        return f"fish {self.index}"

    def __init__(self, idx, locF, locT):
        self.index = idx
        self.locF = (int(locF[0]), int(locF[1]))
        self.locT = (int(locT[0]), int(locT[1]))
        self.roi = None

        self.keypointsF = deque(maxlen=3)
        self.keypointsT = deque(maxlen=3)
        colors = [(0, 0, 255), (0, 255, 0), (255, 0, 0),
                  (255, 255, 0), (0, 255, 255)]
        self.color = colors[idx-1]

    def get_roi(self, img, xy, size=50):
        # Create region of interest mask around x and y coordinates
        x, y = xy
        x = int(x)
        y = int(y)
        gray = img.copy()
        gray[:] = 0
        gray[y-size:y+size, x-size:x+size] = 255
        return gray

    def update(self, img, ft, matcher, top):
        if top:
            roi = self.get_roi(img, self.locT)
            kplist = self.keypointsT
        else:
            roi = self.get_roi(img, self.locF)
            kplist = self.keypointsF

        kps, descriptors = ft.detectAndCompute(img, roi)
        kplist.appendleft((kps, descriptors))
        if len(kplist) < 2:
            return None

        if kplist[1][1] is None or kplist[0][1] is None:
            return

        matches = matcher.knnMatch(
            kplist[0][1], kplist[1][1], k=2)
        # no matches, don't try to update location
        if not matches or len(matches[0]) < 2:
            return None

        if top:
            self.locT = (int(kps[0].pt[0]), int(kps[0].pt[1]))
        else:
            self.locF = (int(kps[0].pt[0]), int(kps[0].pt[1]))
        return kplist[0][0]


class tank:
    def __init__(self, front, top, gt_path):
        self.visResult = vis()
        # self.visTruth = vis(title="Truth")
        self.front = cv.VideoCapture(front)
        self.top = cv.VideoCapture(top)
        self.no_fish = 0
        self.gt = self.process_gt(open(gt_path, 'r').read().splitlines())
        self.alg = cv.ORB_create(
            scaleFactor=2, nfeatures=500, patchSize=31, edgeThreshold=31)
        self.matcher = cv.BFMatcher()
        self.previous_frame = None
        self.frame = 1
        self.fishes = dict()

        self.outputFrameRate = 60.0
        self.videoOut = None

        for num in range(1, self.no_fish+1):

            f = fish(num, self.gt[self.frame, num]
                     [11:13], self.gt[self.frame, num][4:6])
            self.fishes[num] = f

            fCoord = self.coordFromFLoc(f)

            # self.visTruth.addPoint((fCoord), FISH_COLOURS[num-1], num)
            self.visResult.setPoint(num, (fCoord))
            # self.init_keypoints()

    # returns a dict with (frame, fishno) : data
    def process_gt(self, lines):
        arr = [l.split(',') for l in lines]
        for l in arr:
            if int(l[0]) > 1:
                break
            self.no_fish += 1
        gt = {(int(l[0]), int(l[1])): [float(x) for x in l[1:]] for l in arr}
        return gt

    def next_frame(self, top=False):
        if top:
            res, img = self.top.read()
        else:
            res, img = self.front.read()
        self.frame += 1
        if res:
            gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
            fin = img.copy()

            for i in self.fishes:
                fish = self.fishes[i]
                loc = fish.locT if top else fish.locF

                keypoints = fish.update(gray, self.alg, self.matcher, top)
                roi = fish.get_roi(gray, loc)
                roi = cv.cvtColor(roi, cv.COLOR_GRAY2BGR)
                fin = cv.drawKeypoints(fin, keypoints, fin)

                fin = cv.rectangle(
                    fin, (loc[0]-50, loc[1]-50), (loc[0]+50, loc[1]+50), fish.color, thickness=3)
                fin = cv.putText(
                    fin, str(i), loc, cv.FONT_HERSHEY_SIMPLEX, 1, fish.color, 2, cv.LINE_AA)

            fin = cv.resize(fin, (img.shape[1] // 2, img.shape[0] // 2))
            if top:
                self.imOutTop = fin.copy()
                cv.imwrite(f"{BASE_FOLDER_OUT}/imgT/{self.frame:06}.jpg", fin)
                title = "Top"
            else:
                self.imOutFront = fin.copy()
                cv.imwrite(f"{BASE_FOLDER_OUT}/imgF/{self.frame:06}.jpg", fin)
                title = "Front"
            if(DISPLAY):
                cv.imshow(title, fin)
                # ESC or Q to exit
                key = cv.waitKey(1)
                if key == KEY_ESC or key == KEY_Q:
                    return None
                # Any other key to slow playback
                elif(key > 0):
                    time.sleep(0.5)
            self.previous_frame = img
            return True
        else:
            return False

    def ratio_test(self, matches):
        good = []
        for m, n in matches:
            if m.distance < 0.75*n.distance:
                good.append([m])
        return good

    def drawVisualisation(self):

        for i in self.fishes:
            f = self.fishes[i]

            fLoc = self.coordFromFLoc(f)

            self.visResult.setPoint(i, fLoc)
            self.chart3d = self.visResult.savePlot()

    def coordFromFLoc(self, fish):

        x = int((fish.locF[0] + fish.locT[0])/2)
        y = -fish.locT[1]
        z = - fish.locF[1]
        fCoord = (x, y, z)

        return fCoord

    def renderFrame(self):

        # Chart
        self.drawVisualisation()
        chart = cv.imread(VISUALISATION_PATH, cv.IMREAD_COLOR)

        [imgHeightTop, imgWidthTop, _] = np.shape(self.imOutTop)
        [imgHeightFront, imgWidthFront, _] = np.shape(self.imOutFront)
        [imgHeightChart, imgWidthChart, _] = np.shape(chart)
        imgOut = np.zeros(
            (
                max(imgHeightTop, imgHeightFront, imgHeightChart),
                (imgWidthTop + imgWidthFront + imgWidthChart),
                3
            )
        ).astype(np.uint8)

        # Front view
        imgOut[
            0:imgHeightFront,
            0:imgWidthFront
        ] = self.imOutFront[
            0:imgHeightFront,
            0:imgWidthFront
        ]

        # Video writer
        if self.videoOut is None:
            [imgHeightOut, imgWidthOut, _] = np.shape(imgOut)
            self.videoOut = cv.VideoWriter(VIDEO_PATH, cv.VideoWriter_fourcc(
                *'H264'), self.outputFrameRate, (imgWidthOut, imgHeightOut), isColor=True)

        # Top view
        imgOut[
            0:imgHeightTop,
            imgWidthFront:imgWidthFront+imgWidthTop
        ] = self.imOutTop[
            0:imgHeightTop,
            0:imgWidthTop
        ]

        # Chart
        imgOut[
            0:imgHeightChart,
            imgWidthFront + imgWidthTop:imgWidthFront + imgWidthTop + imgWidthChart
        ] = chart[
            0:imgHeightChart,
            0:imgWidthChart
        ]

        self.videoOut.write(imgOut)

        if(DISPLAY):
            cv.imshow("Visualisation", imgOut)

    def closeVideoFile(self):
        self.videoOut.release()


if __name__ == '__main__':
    no = "3"
    vidF_path = DIR + '/../input/labels/train/ZebraFish-0'+no+'/imgF.mp4'
    vidT_path = DIR + '/../input/labels/train/ZebraFish-0'+no+'/imgT.mp4'
    gt_path = DIR + '/../input/labels/train/ZebraFish-0'+no+'/gt/gt.txt'
    t1 = tank(vidF_path, vidT_path, gt_path)
    for i in range(10000):
        print("Frame:", i)
        res = t1.next_frame(top=True)
        if(res):
            t1.next_frame(top=False)
        if res is None or res is False:
            break

        t1.renderFrame()
    t1.closeVideoFile()
